# leaflet_doc

#### 介绍
leallet技术文档

#### 软件架构
Csdn 地址：https://blog.csdn.net/qq_40423339/article/details/106080464?spm=1001.2014.3001.5502

Csdn 地址2: https://blog.csdn.net/qq_40423339/article/details/106098966?spm=1001.2014.3001.5502


#### 安装教程

1.  下载node
2.  npm install 
3.  运行 npm run dev

#### 使用说明

1.  访问 ip+port/notebook1
2.  访问 ip+port/notebook2
3.  访问 ip+port/notebook3
4.  访问 ip+port/notebook4



# 下载leaflet与leaflet.pm
leaflet 用于根据坐标自动绘制等操作，leaflet.pm可实现用户手动绘制。
leaflet.pm 参考网址：https://www.npmjs.com/package/leaflet.pm
leaflet 参考网址：https://leafletjs.com/reference-1.6.0.html
```
npm install leaflet.pm
npm install leaflet
```
# 引入，在项目main.js中
```js
import 'leaflet/dist/leaflet.css'
// 引入Leaflet对象 挂载到Vue上，便于全局使用，也可以单独页面中单独引用
import * as L from 'leaflet'
import 'leaflet.pm'
import 'leaflet.pm/dist/leaflet.pm.css'

Vue.config.productionTip = false;
Vue.L = Vue.prototype.$L = L

/* leaflet icon */
delete L.Icon.Default.prototype._getIconUrl
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
})

```
# 初始化地图
## 初始化
```
 map = L.map('map',{
      center: [40.02404009136253, 116.50641060224784], // 地图中心
      zoom: 14,   //缩放比列
      zoomControl: false, //禁用 + - 按钮
      doubleClickZoom: false,  // 禁用双击放大
      attributionControl: false  // 移除右下角leaflet标识
    });
```
## 引入图层,可以引入多个图层
```js
 L.tileLayer(
    "http://mt0.google.cn/vt/lyrs=y@160000000&hl=zh-CN&gl=CN&src=app&y={y}&x={x}&z={z}&s=Ga",
  ).addTo(this.map);
```

## 初始化综合代码
```js
<template>
    <div>
    <div id="map"></div>
  </div>
</template>

<script>
export default {
  data() {
    return {
        map: ""
    };
  },
  mounted() {
    this.initDate();
  },
  methods: {
    initDate() {
      this.map = L.map("map", {
        center: [40.02404009136253, 116.50641060224784], // 地图中心
        zoom: 14, //缩放比列
        zoomControl: false, //禁用 + - 按钮
        doubleClickZoom: false, // 禁用双击放大
        attributionControl: false // 移除右下角leaflet标识
      });
      let name = L.tileLayer(
        "http://mt0.google.cn/vt/lyrs=y@160000000&hl=zh-CN&gl=CN&src=app&y={y}&x={x}&z={z}&s=Ga",
      ).addTo(this.map);
    //   this.map.removeLayer(name)  // 移除图层
    }
  }
};
</script>

<style lang="stylus" scoped>
#map {
  width: 100%;
  height: calc(100vh);
  z-index: 1;
}
</style>
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200512173130272.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQwNDIzMzM5,size_16,color_FFFFFF,t_70#pic_center)
# 添加绘制按钮功能及获取对应坐标
## 引入leafle.pm原生组件
```js
 this.map.pm.addControls({
    position: "topleft",
    drawPolygon: false, // 添加绘制多边形
    drawMarker: false, //添加按钮以绘制标记
    drawCircleMarker: false, //添加按钮以绘制圆形标记
    drawPolyline: false, //添加按钮绘制线条
    drawRectangle:	true,	//添加按钮绘制矩形
    drawCircle: false, //  添加按钮绘制圆圈
    editMode: true, //  添加按钮编辑多边形
    dragMode: true, //   添加按钮拖动多边形
    cutPolygon: true, // 添加一个按钮以删除图层里面的部分内容
    removalMode: true  // 清除图层
  });
 // 设置绘制后的线条颜色等
this.map.pm.setPathOptions({
    color: "orange",
    fillColor: "green",
    fillOpacity: 0.4
});
this.map.pm.setLang('zh');  //设置语言  en, de, it, ru, ro, es, fr, pt_br, zh , nl
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200512173146538.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQwNDIzMzM5,size_16,color_FFFFFF,t_70#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/202005121818087.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQwNDIzMzM5,size_16,color_FFFFFF,t_70#pic_center)
## 绑定自己的按钮
```js
<!----template>
<template>
  <div>
    <button class="draw" @click="draw()">绘制</button>
    <button class="disDraw" @click="disDraw()">关闭绘制</button>
    <div id="map"></div>
  </div>
</template>

<!----js>
draw() {
  this.map.pm.enableDraw("Polygon", { 
      snappable: false,
   });
  //   this.map.pm.enableDraw("Marker", { snappable: false });
  //   this.map.pm.enableDraw("CircleMarker", { snappable: false });
},
disDraw() {
  this.map.pm.disableDraw("Polygon");
  //    this.map.pm.disableDraw('Marker');
  //    this.map.pm.disableDraw('CircleMarker');
}

<!----style>
.draw {
  display: flex;
  z-index: 2;
  width: 100px;
  height: 50px;
  position: absolute;
  left: 50px;
  justify-content: center;
  align-items: center;
}

.disDraw {
  display: flex;
  z-index: 2;
  width: 100px;
  height: 50px;
  position: absolute;
  left: 200px;
  justify-content: center;
  align-items: center;
}
```
## 获取绘制的坐标
```js
getlatLngs() {
  //pm:drawstart 开始第一个点的时候调用
  //pm:drawend  禁止绘制时调用
  //pm:create  创建完成时调用
  this.map.on("pm:drawstart", e => {
    console.log(e, "first");
  });
  this.map.on("pm:drawend", e => {
    console.log(e, "禁止绘制");
  });
  this.map.on("pm:create", e => {
    console.log(e, "绘制完成时调用");
    console.log(e.layer._latlngs[0], "绘制坐标")
  });
},
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200512173203915.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQwNDIzMzM5,size_16,color_FFFFFF,t_70#pic_center)
# 编辑功能
## 绘制完成添加可编辑功能
layer为需要改变的图层
```
layer.pm.enable({
    allowSelfIntersection: false, 
    preventMarkerRemoval: false,  // 禁止右键删除点
});
```
## 监听编辑事件
```
layer.on('pm:edit', e => {
    console.log(e, "拖动");
    console.log(e.target._latlngs[0], "拖动后的坐标")
});
layer.on('pm:edit', e => {
    console.log(e, "拖动");
    console.log(e.target._latlngs[0], "拖动后的坐标")
});
layer.on('pm:vertexadded', e =>{
    console.log(e, "添加顶点")
});
```
## 全局编辑开启关闭
```
 // 开启全体编辑按钮
map.pm.toggleGlobalEditMode();

// 禁用全局编辑按钮
map.pm.disableGlobalEditMode()


// 全局编辑切换
map.pm.toggleGlobalEditMode()

// 判断是否全局编辑,有返回值
map.pm.globalEditEnabled()

```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200512173224304.gif#pic_center)
## 拖动
```js
map.pm.toggleGlobalDragMode()
// 是否启用全局拖动模式
alert(map.pm.globalDragModeEnabled())
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020051217433926.gif#pic_center)
## 删除
```js
map.pm.toggleGlobalRemovalMode();
```
## 切割
```
//开启
map.pm.Draw.Cut.enable({
    allowSelfIntersection: false
});

// 关闭
map.pm.Draw.Cut.disable()

// 切换
map.pm.Draw.Cut.toggle();

// 监听切割事件
layer.on("pm:cut", e =>{
     console.log(e, "切割");
})
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200512175221414.gif#pic_center)

[vue中使用leaflet(二)](https://blog.csdn.net/qq_40423339/article/details/106098966)

## 根据坐标绘制矩形
```
let latlngs = [
        [40.0214690112063, 116.50239229202272],
        [40.019694293123855, 116.50224208831787],
        [40.01979288978388, 116.50580406188966],
        [40.021436146476105, 116.50601863861085],
        [40.02253710631967, 116.50316476821901]
      ];
 this.drawObj = L.polygon(latlngs, { color: "red" }).addTo(this.map);
 this.drawObj.bindTooltip('this is 个多边形');
// color：线段颜色
// weight：线段宽度
// opacity：线段透明度
// dashArray：虚线间隔
// fill：是否填充内部(true/false)
// fillColor:内部填充颜色，如不设置，默认为color颜色
// fillOpacity：内部填充透明度
 this.drawRadius = L.circle([40.0224690112063, 116.51339229202272], {
        radius: 200
      }).addTo(this.map);
```
## 开启绘制图像的编辑功能
```
this.drawObj.pm.enable({
    allowSelfIntersection: false
});
this.drawRadius.pm.enable({
    allowSelfIntersection: false
});
```
## 监听绘制后的坐标
```
this.drawObj.on("pm:edit", obj => {
    obj.target.setStyle({ color: "orange" });
    console.log(obj.target._latlngs[0]);
});

this.drawRadius.on("pm:edit", obj => {
    obj.target.setStyle({ color: "orange" });
    console.log(obj.target._latlng);
});
```
## 添加点击事件
```
this.drawObj.on("keydown", e=>{
    alert("我是矩形点击事件")
});
this.drawRadius.on("click", e=>{
    alert("我是圆形点击事件")
});
/*
click: 点击事件
dblclick：双击	
mousedown：按下鼠标按钮
mouseup：释放鼠标按钮
mouseover：鼠标进入
mouseout：鼠标离开
mousemove：鼠标移入
contextmenu：鼠标右键
preclick：点击之前
*/
```
## 移除点击事件
```
this.drawObj.off("click")
this.drawRadius.off("click")
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200513152006186.gif#pic_center)
## 将图行置为顶层
```
    changePplygon() {
      this.drawRadius.bringToBack(); // 置为顶层
      //   this.drawObj.bringToFront(); // 置为底层
    },
```
## 将修改完的多边形还原
```
this.drawObj.setLatLngs([
    [40.0214690112063, 116.50239229202272],
    [40.019694293123855, 116.50224208831787],
    [40.01979288978388, 116.50580406188966],
    [40.021436146476105, 116.50601863861085],
    [40.02253710631967, 116.50316476821901]
]);
this.drawObj.setStyle({ color: "red" });
this.drawObj.pm.disable();
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020051315202551.gif#pic_center)
## 获取对象的边界值
```
this.drawObj.getLatLngs()
```
[vue中使用leaflet(二)](https://blog.csdn.net/qq_40423339/article/details/106080464)

